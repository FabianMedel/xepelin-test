import { DocumentBuilder } from '@nestjs/swagger';

export const swaggerConfig = new DocumentBuilder()
    .setTitle('Invoices api REST')
    .setDescription('Xepelin test')
    .setVersion('1.0')
    .setBasePath('/api/v1')
    .build();