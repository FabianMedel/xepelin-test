export const dateStringToDate = (dateString: string): Date => {
    const dateParts = dateString.split('-').map(
        (value: string): number => {
            return parseInt(value);
        }
    );
    
    return new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
}

export const formatAmount = (amount: number): any => {
    return new Intl.NumberFormat('en-US').format(amount)
}

export const addDaysToDate = (startDate: Date, numberDays: number): Date => {
    return new Date(
        startDate.setDate(startDate.getDate() + numberDays)
    );
}

export const zip = (a,b) => a.map((x,i) => [x,b[i]]);

export const uniqs = (arr) => [... new Set(arr)];

export const numbersSort = (arr) => arr.sort(function(a, b) {
    return a - b;
});

export const largestSort = (arr) => arr.sort(function(a, b) {
    return b.largestNetwork - a.largestNetwork;
});