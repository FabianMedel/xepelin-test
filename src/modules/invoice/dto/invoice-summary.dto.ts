import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class InvoiceSummaryDto { 
    @ApiProperty({
        example: 1,
        type: Number,
        required: true
    })
    @IsNotEmpty()
    @IsNumber()
    business_id: number;

    @ApiProperty({
        example: '2022-05-01',
        type: String,
        required: true
    })
    @IsNotEmpty()
    @IsString()
    start_date: string; 
}