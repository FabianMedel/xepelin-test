import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class NetworkCompaniesDto { 
    @ApiProperty({
        example: '2022-05-01',
        type: String,
        required: true
    })
    @IsNotEmpty()
    @IsString()
    start_date: string;

    @ApiProperty({
        example: 5,
        type: Number,
        required: true
    })
    @IsNotEmpty()
    @IsNumber()
    limit_top: number;

}