import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsNotEmpty, IsNumber } from "class-validator";

export class InvoiceDto {
    @ApiProperty({
        example: 1,
        type: Number,
        required: true
    })
    @IsNotEmpty()
    @IsNumber()
    invoice_id: number;

    @ApiProperty({
        example: '2018-11-19',
        type: Date,
        required: true
    })
    @IsNotEmpty()
    @IsDate()
    issueDate: Date;

    @ApiProperty({
        example: '2018-11-19',
        type: Date,
        required: true
    })
    @IsNotEmpty()
    @IsDate()
    paymentDate: Date;

    @ApiProperty({
        example: 48,
        type: Number,
        required: true
    })
    @IsNotEmpty()
    @IsNumber()
    issuer_id: number;

    @ApiProperty({
        example: 14,
        type: Number,
        required: true
    })
    @IsNotEmpty()
    @IsNumber()
    receiver_id: number;

    @ApiProperty({
        example: 245,
        type: Number,
        required: true
    })
    @IsNotEmpty()
    @IsNumber()
    amount: number; 
}
