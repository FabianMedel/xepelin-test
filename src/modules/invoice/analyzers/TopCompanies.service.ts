import { addDaysToDate, dateStringToDate, formatAmount, largestSort, numbersSort, uniqs, zip } from "../../utils/utils";
import { MatchData } from "../../data/csv";
import { Analyzer } from "./interfaces/Invoice";

export class TopCompanies implements Analyzer {
    private issuers: any;
    private receivers: any;
    private companies: any;

    constructor(
        public startDate: string,
        public limitTop: number
    ) {}
    
    run(matches: MatchData[]): MatchData[] {
        this.companiesSort(matches);
        return this.statsCompanies(matches);
    }

    statsCompanies(matches: MatchData[]) {
        let clientAndProviderSums = [];
        let companiesNetworks = [];
        let companiesNetworksSort = [];
        let isClientAndProvider = false;

        const startDate = dateStringToDate(this.startDate);
        const endDate = addDaysToDate(dateStringToDate(this.startDate), 30)
        
        for (let company of this.companies) {
            let sumIssuer = 0;
            let sumReceiver = 0;
            
            for (let match of matches) {
                if(company === match[3] && (match[1] > startDate && match[1] < endDate))
                    sumIssuer += match[5];
                if(company === match[4] && (match[1] > startDate && match[1] < endDate))
                    sumReceiver += match[5];
                if(company === match[3] && company === match[4])
                    isClientAndProvider = true;
            }
            clientAndProviderSums.push(sumIssuer + sumReceiver)
        }

        for (let [bussines_id, largestNetwork] of zip(this.companies, clientAndProviderSums)) {
            companiesNetworks.push({
                bussines_id,
                largestNetwork,
                isClientAndProvider
            });
        }
        companiesNetworksSort = largestSort(companiesNetworks);
       
        return this.extractTopCompanies(
            companiesNetworksSort, 
            this.limitTop
        );
    }

    extractTopCompanies(companies: any, limit: number) {
        let topResult = [];

        companies.map(x => { x.largestNetwork  = formatAmount(x.largestNetwork)});

        for(let i = 0 ; i < limit ; i++) {
            topResult.push(companies[i]);
        }
        return topResult;
    }
    
    companiesSort(matches: MatchData[]): void{
        let issuers = [];
        let receivers = [];
        
        for(let match of matches){
            if(!(isNaN(match[3])) || !(isNaN(match[4]))) {
                issuers.push(match[3]);
                receivers.push(match[4]);
            }
        }
        
        this.issuers = uniqs(issuers);
        this.receivers = uniqs(receivers);

        this.companies = uniqs(
            numbersSort(
                this.issuers.concat(this.receivers)
            )
        );
    }
}