import { addDaysToDate, dateStringToDate, formatAmount } from '../../utils/utils';
import { MatchData } from "../../data/csv";
import { Analyzer } from "./interfaces/Invoice";

export class Summary implements Analyzer {
    constructor(
        public bussinesId: number,
        public startDate: string
    ) {}

    run(matches: MatchData[]): any {
        const nextSeven = this.statsNextDays(matches, 8);
        const nextfiveteen = this.statsNextDays(matches, 15);
        const nextThirty = this. statsNextDays(matches, 31)
        return {
            bussines_id: this.bussinesId,
            start_date: this.startDate,
            resume: {
                next_seven: nextSeven,
                next_fiveteen: nextfiveteen,
                next_Thirty: nextThirty
            }
        };
    }

    statsNextDays(matches: MatchData[], numberDays: number) {
        let incomes = 0;
        let expenses = 0;
        const startDate = addDaysToDate(dateStringToDate(this.startDate), 1);
        const endDate = addDaysToDate(dateStringToDate(this.startDate), numberDays)
        
        for(let match of matches) {
            if(match[3] === this.bussinesId && (match[2] > startDate && match[2] < endDate)) {
                incomes += match[5];
            }
            if(match[4] === this.bussinesId && (match[2] > startDate && match[2] < endDate)) {
                expenses += match[5];
            }
        }
        return {
            expenses: formatAmount(expenses),
            incomes: formatAmount(incomes)
        }
    }
}