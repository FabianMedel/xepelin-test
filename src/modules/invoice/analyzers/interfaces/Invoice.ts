import { MatchData } from "../../../data/csv";

export interface Analyzer {
    run(matches: MatchData[]): MatchData[];
}

export class Invoice {
    constructor(
        public analyzer: Analyzer,
    ) {}

    build(matches: MatchData[]): any {
        const output = this.analyzer.run(matches);
        return output
    }
}