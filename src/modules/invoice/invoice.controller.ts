import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { InvoiceService } from './invoice.service';
import { ApiTags } from '@nestjs/swagger';
import { InvoiceDto, InvoiceSummaryDto, NetworkCompaniesDto } from './dto';

@Controller('invoice')
@ApiTags('Invoice')
export class InvoiceController {
  constructor(private readonly invoiceService: InvoiceService) {}

  @Post('/summary')
  summaryBussinesByID(@Body() invoiceSummary: InvoiceSummaryDto) {
    return this.invoiceService.summaryBussinesByID(invoiceSummary);
  }

  @Post('/top-companies')
  create(@Body() topCompanies: NetworkCompaniesDto) {
    return this.invoiceService.largestNetworkCompanies(topCompanies);
  }

}
