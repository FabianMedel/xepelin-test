import { Test, TestingModule } from '@nestjs/testing';
import { MatchData } from '../data/csv';
import { dateStringToDate } from '../utils/utils';
import { Summary, TopCompanies } from './analyzers';
import { InvoiceSummaryDto, NetworkCompaniesDto } from './dto';
import { InvoiceService } from './invoice.service';

const mockSummaryService = () => ({
  run: jest.fn(),
});

const mockTopCompaniesService = () => ({
  run: jest.fn(),
});


const mockInvoice: MatchData[] = [
  [0, dateStringToDate('2022-05-01'), dateStringToDate('2022-05-17'), 0,45,2500],
  [1, dateStringToDate('2022-05-09'), dateStringToDate('2022-05-20'), 1,17,500],
  [2, dateStringToDate('2022-05-31'), dateStringToDate('2022-06-01'), 12,0,200],
  [3, dateStringToDate('2022-06-01'), dateStringToDate('2022-07-11'), 13,,25000],
  [4, dateStringToDate('2022-12-01'), dateStringToDate('2022-05-10'), 146,13,2500]
]


describe('InvoiceService', () => {
  let invoiceService: InvoiceService;
  let summaryService;
  let topCompaniesServices;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        InvoiceService,
        { provide: Summary, useFactory: mockSummaryService },
        { provide: TopCompanies, useFactory: mockTopCompaniesService },
      ],
    }).compile();

    invoiceService = module.get(InvoiceService);
    summaryService = module.get(Summary);
    topCompaniesServices = module.get(TopCompanies);
  });

  
  describe('summaryBussinesByID', () => {
    it('calls run() and returns the result', async () => {
      const summary: any = {
        bussines_id: 0,
        start_date: "2022-05-01",
        resume: {
            next_seven: {
                expenses: "67,739",
                incomes: "36,479"
            },
            next_fiveteen: {
                expenses: "155,957",
                incomes: "102,197"
            },
            next_Thirty: {
                expenses: "364,236",
                incomes: "106,647"
            }
        }
    }

      const params: InvoiceSummaryDto = {
        business_id: 0,
        start_date: '2022-05-01'
      }

      summaryService.run.mockResolvedValue(summary);
      const result = invoiceService.summaryBussinesByID(params);
      expect(result).toEqual(summary)
    });
  });

  describe('largestNetworkCompanies', () => {
    it('calls run() and returns the result', async () => {
      const mockTopCompanies: any = [
        {
          bussines_id: 17,
          largestNetwork: "172,217,116",
          isClientAndProvider: true
        },
        {
          bussines_id: 78,
          largestNetwork: "132,147,107",
          isClientAndProvider: true
        },
        {
          bussines_id: 142,
          largestNetwork: "67,019,187",
          isClientAndProvider: true
        },
        {
          bussines_id: 2,
          largestNetwork: "48,776,816",
          isClientAndProvider: true
        },
        {
          bussines_id: 91,
          largestNetwork: "23,703,476",
          isClientAndProvider: true
        }
      ]

      const params: NetworkCompaniesDto = {
        start_date: '2022-05-01',
        limit_top: 5
      }

      summaryService.run.mockResolvedValue(mockTopCompanies);
      const result = invoiceService.largestNetworkCompanies(params);
      expect(result).toEqual(mockTopCompanies)
    });
  });
});
