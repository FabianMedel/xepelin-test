import { Injectable } from '@nestjs/common';
import { Invoice } from './analyzers/interfaces/Invoice';
import { Summary, TopCompanies } from './analyzers';
import { CsvFileReader, MatchData, MatchReader } from '../data/csv';
import { InvoiceDto, InvoiceSummaryDto, NetworkCompaniesDto } from './dto';
import { dateStringToDate } from '../utils/utils';

@Injectable()
export class InvoiceService {
  csvFileReader: CsvFileReader;
  matchReader: MatchReader;
  
  constructor() {
    this.csvFileReader = new CsvFileReader('invoices.csv');
    this.matchReader = new MatchReader(this.csvFileReader);
    this.matchReader.load();
  }

  largestNetworkCompanies({ start_date, limit_top }: NetworkCompaniesDto) {
    const topCompanies = new Invoice(
      new TopCompanies(start_date, limit_top)
    );
   
    return topCompanies.build(this.matchReader.matches);
  }

  summaryBussinesByID({business_id, start_date}: InvoiceSummaryDto) {
    const invoiceSummary = new Invoice(
      new Summary(business_id, start_date),
    );

    return invoiceSummary.build(this.matchReader.matches);
  }

}
