import { dateStringToDate } from '../../utils/utils';
import { DataReader } from './interfaces/DataReader.interface';
import { MatchData } from './types';

export class MatchReader {
    matches: MatchData[] = [];
    constructor(public reader: DataReader) {}

    load(): void {
        this.reader.read();
        this.matches = this.reader.data.map(
            (row: string[]): MatchData => {
                return [
                    parseInt(row[0]),
                    dateStringToDate(row[4]),
                    dateStringToDate(row[5]),
                    parseInt(row[1]),
                    parseInt(row[2]),
                    parseInt(row[3]),
                ];
            }
        )
    }
}