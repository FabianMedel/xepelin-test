FROM public.ecr.aws/bitnami/node:14-prod

WORKDIR  /usr/src/app

COPY package*.json ./
RUN npm install -g npm
RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]

